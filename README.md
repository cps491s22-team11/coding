University of Dayton

Department of Computer Science

CPS 491 - Fall 2022

Dr. Phu Phung


## Capstone II Proposal 



# Project Topic

Translation plug-in

# Team members



1.  Mingqing Luo 1, luom02@udayton.edu





# Project Management


![Management board (private access): ](https://trello.com/1/cards/61f90d597f073c34e54434b5/attachments/61f90d597f073c34e54434e9/download/20220201053635.png)

![Source code repository (private access):][https://bitbucket.org/cps491s22-team11/coding/src/master/]

![Project homepage (public): ] [https://cps491s22-team13.bitbucket.io/]

# Revision History

| Date     | Version | Description     |
| ---    |    :----:   |    --- |
| 2/01/2022     | 0.0      | initial phrase   |


# Overview

Help users who use messenger can use different voice barrier free communication.


![Overview Architecture](https://trello.com/1/cards/620223f702dc3842297a086d/attachments/620223f702dc3842297a0898/download/A.png)

Figure 1. - A Sample of Overview Architecture of the proposed project.

# Project Context and Scope

Firstly, translation tools will be used as a function of messenger to help people with different native languages communicate freely on messenger.
As international students, we hope that people from different geographical locations in the world can communicate more


# System Analysis

# High-level Requirements

Realize the usual translation function.

Change the translation function to a plug-in, which can run on different web pages

Locate the words to be translated through the mouse

### User requirement

1. The user can input the content he wants to send

2. Users can use the translation function

3. Users can get the translation results

### System requirement

1. The system can be connected to Baidu translation API

2. The system can send the content to be translated to Google translation API

3. The system can receive the translation results

4. The system will show the translation results to users

## Use cases

### Overview use case diagram

![timeline](https://trello.com/1/cards/61f993c4b7ac062ce5f5485a/attachments/61f993c4b7ac062ce5f54896/download/123.png)

Use case descriptions

1.user can translate the chat they want to translate

### System Design

1.The system will extract data from the chat box and send it to Google translation API. 
Google translation API will translate according to the input content, and finally return the translation result.
![case](https://trello.com/1/cards/620223202b4850306ff31ba4/attachments/620223202b4850306ff31bce/download/20220208030005.png)

2.The html file will send request to the back-end server which include controller, service and mapping layer.
![case2](https://trello.com/1/cards/61f997ba3a071c0ecd8f15fb/attachments/61f997ba3a071c0ecd8f1639/download/20220201152627.png)

## Database


# User Interface
![Implementation](https://trello.com/1/cards/620221ea18f65d548a52a254/attachments/620221ea18f65d548a52a282/download/20220208025507.png)
user can input the word or sentence by chinese and get the result of english,japanese,Korean,French,Russian

![Implementation](https://trello.com/1/cards/621d893f832cf34214ea6ad1/attachments/621d893f832cf34214ea6b01/download/20220228214703.png)
Interface 2.0
Add animation effect to the button, and now users can accurately know whether they have successfully clicked the button
# Implementation


![Implementation1](https://trello.com/1/cards/620beb80158b005da81567d6/attachments/620beb80158b005da8156804/download/20220215130540.png)
When the user enters something in the translation input text box, it will get it and find the language entered by the user.
![Implementation2](https://trello.com/1/cards/620bea4c9b56587a345ccb33/attachments/620bea4c9b56587a345ccb60/download/20220215130005.png)
The server will convert the input text and the language to be translated, as well as my baidu translation developer ID and secret key into MD5 format and send it to the API. Also with a random number for callback name.
![Implementation3](https://trello.com/1/cards/620bed206653db54a7d25f80/attachments/620bed206653db54a7d25faf/download/20220215131230.png)
send the result to the index
![Implementation4](https://trello.com/1/cards/621d8a61fc6e2264458388b5/attachments/621d8a61fc6e2264458388e6/download/20220228215024.png)
Users can now automatically enter text into the input text box by selecting text with the mouse instead of typing themselves.
![Implementation5](https://trello.com/1/cards/621d8aeaf95e74359db00b90/attachments/621d8aeaf95e74359db00bc2/download/20220228215200.png)
Through CSS animation, users can know whether they have successfully clicked the button
# Technology	

index.html

Node.js

ajax

Baidu translation API

CSS

jquery

# Impacts

The user can translate the articles in any open web page into the language he uses.


# Software Process Management

Include the screenshot of the timeline from your Trello board (with tasks). You can use the Trello template available here (only with timeline): [https://trello.com/b/ekNpUFWG/translation-plug-in]



# Support

Dr. Phu Phung (phu@udayton.edu )



### Sprint 0

Duration: 21/01/2022-01/02/2022

#### Completed Tasks: 

1.Make trello schedule

2.Edit bitbickt; Readme

3.Reload the messenger software and download the required software (such as socket.io, NPM install -- save, Google Translate API, etc.)

4.Try loading the Google Translate API into the messenger runtime

#### Contributions: 

1.  Member 1, Mingqing Luo, Make trello schedule,bitbickt;README,Reload the messenger software and download the required software y hours,Try loading the Google Translate API into the messenger runtime contributed in 20h

### Sprint 2-3
Duration: 02/02/2022-08/02/2022

#### Completed Tasks: 

1.Investigate and register translation APIs.

2.Link API, and test whether to connect to API through using MD5 character.

3.Design index, download css template.

4.Write a demo. Including sending a request in md5 format to the API, converting Chinese characters into computer characters, and writing them in md5 to send as sign. Get the API response and display it on the index.

5.Debug

#### Contributions: 

1.  Member 1, Mingqing Luo，From investigating the registration API, to establishing an index and demo for connecting to the API and write the report 18h.

### Sprint 4
Duration: 08/02/2022-17/02/2022

#### Completed Tasks: 

1.Learn simple CSS usage

2.Use CSS to beautify the interface (including beautifying header, botton, background, etc.)

3.Use CSS to add click animation to the button, so that users can accurately know that they have successfully clicked the button

#### Contributions: 

1.  Member 1, Mingqing Luo，Learn to use CSS, beautify the interface with CSS, and add click animation to the button. 7h

### Sprint 5
Duration: 18/02/2022-25/2/2022

#### Completed Tasks: 

1.Fixed the previous bug. Now the translation tool can translate English into four other languages and automatically switch languages

2.Search how to implement the event of detecting mouse selection in HTML

3.Modify all bugs in the program so that the interface can run beautifully and smoothly

4.Update Readme

#### Contributions: 

1.  Member 1, Mingqing Luo，Modify the original bug of JS. Now English can be translated into four other languages. Search the implementation method of mouse selection event in HTML, edit and update readme  8h


### Sprint 6
Duration: 26/02/2022-04/03/2022

#### Completed Tasks: 

1 Realize the application of mouse select event in HTML. Now the program can automatically detect any text selected by the mouse and automatically input it into the input text box

2.Try to use the mouse up event to automatically complete the text translation (not yet completed)

3.Search about how to build web plug-ins


#### Contributions: 

1.  Member 1, Mingqing Luo，Detect the mouse select event, automatically input the coding of the text, and try to automatically translate the text. Construction of search web plugin. 11h
