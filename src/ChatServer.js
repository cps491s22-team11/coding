var http = require('http')
var app = require('express')()
var server = http.createServer(app)
const port = process.env.PORT || 8080
server.listen(port);
console.log(`Express HTTP Server is listening at port ${port}`)
app.get('/', (request, response) => {
  console.log("Got an HTTP request")  
  response.sendFile(__dirname+'/index.html')
})
var io = require('socket.io');
var socketio = io.listen(server);
var userlists = [];
var mName2id =[];
function validateUsername(username){
//a simple input validation requiring the username must be 2 chars or longer
return (username && username.length > 2);
}
function validatePassword(password){
//a validation requiring the password must be 6 chars or longer
//must contain at least one digit, one lower case, and one UPPERCASE
return /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/.test(password);
}
console.log("Socket.IO is listening at port: "+port);
socketio.on("connection", function (socketclient) {
    console.log("A new Socket.IO client is connected. ID= "+ socketclient.id);
    socketclient.on("register",(username,password,age,college,gender)=>{
        if(validateUsername(username)&&validatePassword(password)){
        DataLayer.addUser(username,password,age,college,gender,(result)=>{
            socketclient.emit("registration",result)
        }
        )
        }
        else{
            result="";
            var alert = "Invalid register";
            console.log(alert);
            socketclient.emit("registration",result);
        }
    })
    
    socketclient.on('login', async (username,password)=>{
        console.log("Debug>Got username="+username + ";password="+password);
        var checklogin = await DataLayer.checklogin(username,password)
        if(checklogin !== null)
        {
            socketclient.authenticated = true;
            let data = {
                  username: username,
                  welcomemessage:  welcomemessage,
                  groupId: checklogin.groupid
                }
            socketclient.emit("authenticated");
            socketclient.username = username;
            var user = username;
            var usermod ={};
            mName2id[username]=socketclient.id;
            usermod.username= username;
            usermod.id = socketclient.id;
            console.log(userlists.push(username));
            var welcomemessage = username + " has joint the chat system!";
            SendToAuthenticatedClient(socketclient,"welcome",welcomemessage,data);
            console.log(welcomemessage);
            //socketio.sockets.emit("welcome", welcomemessage);
            socketclient.emit("User",userlists);
            var Goup_history = await messengerdb.loadGoupChatHistory(username)
                if(Goup_history&&Guop_history.length > 0){
                    Goup_history = chat_history.reverse();  //reverse the order as we get the latest first
                    socketclient.emit("Goup_history",Goup_history);
                }
            var chat_history = await messengerdb.loadPriChatHistory(username)
                if (chat_history && chat_history.length > 0)
                {
                    chat_history = chat_history.reverse();  //reverse the order as we get the latest first
                    socketclient.emit("chat_history",chat_history);
                }        
        }
        else
        {
            socketclient.authenticated = false;          
            socketclient.emit("Unauthenticated");
            var alert = "Invalid input";
            console.log(alert);
        }
    });
    socketclient.on("refreshOnlineUser", ()=>{
        var str = JSON.stringify(userlists);
        console.log(userlists.length+","+str+" refreshOnlineUser");
        socketio.sockets.emit("userlist", str);
    });
     socketclient.on("leave", ()=>{
         
        var logOutmessage = socketclient.username + " has left the chat";
        console.log(logOutmessage);
        socketio.sockets.emit("disconnect", logOutmessage);
    });
    socketclient.on("chat", (message)=>{
        if(!socketclient.authenticated) {
            console.log("Unauthenticated client sent a chat. Suppress!");
            return;
        }
        var chatmessage = socketclient.username + " says: " + message;
        console.log(chatmessage);
        //socketio.sockets.emit("chat", chatmessage);
        SendToAuthenticatedClient(undefined,"chat",chatmessage);
    });
    socketclient.on("groupChat", (message, groupId)=>{
          if(!socketclient.authenticated) {
              console.log("Unauthenticated client sent a chat. Suppress!");
              return;
          }
          var chatmessage =  " says: " + message;
          console.log(chatmessage);
          //socketio.sockets.emit("chat", chatmessage);
          let data = {
            message: chatmessage,
            groupId: groupId
          }
          SendToAuthenticatedClient(undefined,"groupChat",data);
      })

  socketclient.on("queryUser", async () => {
        let userlist = await DataLayer.queryUser();
        console.log(userlist)
        SendToAuthenticatedClient(socketclient,"queryUser",userlist);           
    })

    socketclient.on("addGroup", async (checkedList) => {
        let groupId = await DataLayer.addGroup(checkedList);
        SendToAuthenticatedClient(socketclient,"addGroupSuccess",groupId);           
    })
    socketclient.on("<TYPE>", function(){
        /*if(isNullOrUndefined(socketclient.username)) {return;}*/
        var msg= socketclient.username;
        socketio.sockets.emit("<TYPING>",msg);
        console.log("[<TYPING>," + msg + "] is sent to all connected clients");
    })


    socketclient.on("friendinformation",async (name)=>{
          var userage= await DataLayer.getUserAge(name)
          var usergender=await DataLayer.getUserGender(name);
          var usercollege=await DataLayer.getUserCollege(name);;
          socketio.sockets.emit("getuserinfromation", userage,usergender,usercollege);
          
    })
     socketclient.on("Guopchat",async  (s,myname)=>{
        var mod = JSON.parse(s);
        console.log("check group username : " + mod["groupId"]);
        var username= await DataLayer.getGruoprember(mod["groupId"])
        var usermessenger=mod["messenger"];
        var ps = myname + ": "+ usermessenger;

        for(let i = 0,len = username.length;i <= len;i++){
        console.log("check username at Groupchat: "+ username[i]);
        var id = mName2id[username(i)];
        socketio.to(id).emit('toGuopchat', s);
        }
        messengerdb.storeGruopChat(username,ps);
    });  
    socketclient.on("prichat", (s,myname)=>{   
        var mod = JSON.parse(s);
        var username=mod["name"];
        var usermessenger=mod["messenger"];
        var id = mName2id[username];
        var ps = myname + ": "+ usermessenger;
        socketio.to(id).emit('toprichat', s);
        messengerdb.storePrivateChat(username,ps);
    });
    var messengerdb=require("./messengerdb")
    var DataLayer = {
        info: 'Data Layer Implementation for Messenger',
       async checklogin(username,password){
           var checklogin_result =
                       await messengerdb.checklogin(username,password)
           console.log("Debug>DataLayer.checklogin->result="+
                        checklogin_result)
           return checklogin_result
        },
        async queryUser(){
          return messengerdb.queryUser();
        },
        async addGroup(userlist){
           return await messengerdb.addGroup(userlist);
        }
        ,
        async getUserAge(username){
            var getUserage_result=await messengerdb.getUserAge(username)
            return getUserage_result
        }
        ,
        async getUserCollege(username){
            var getUsercollege_result=await messengerdb.getUserCollege(username)
            return getUsercollege_result
        }
        ,
        async getUserGender(username){
            var getUsergender_result=await messengerdb.getUserGender(username)
            return getUsergender_result
        }
        ,
        async getGruoprember(groupId){
            var getGruoprember_result=await messengerdb.getGruoprember(groupId)
            return getGruoprember_result
        }
        ,
        async addUser(username,password,age,college,gender,callback) {
            messengerdb.addUser(username,password,age,college,gender, (result)=>{
                callback(result)
            }
            )
        }
    }


    function SendToAuthenticatedClient(sendersocket,type,data){
        var sockets = socketio.sockets.sockets;
        for(var socketId in sockets){
            var socketclient = sockets[socketId];
            if(socketclient.authenticated){
                socketclient.emit(type,data);
                var logmsg= "Debug:>sent to " +
                    socketclient.username + " with ID=" + socketId;
                console.log(logmsg);
            }
        }
    }

});
