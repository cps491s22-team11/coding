const MongoClient = require('mongodb').MongoClient;
const uri = "mongodb+srv://jiangh8:jhw888@messengerdb.jfbrr.mongodb.net/messenger?retryWrites=true&w=majority";
const mongodbclient = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
const bcrypt = require("bcryptjs")

let db = null;
mongodbclient.connect( (err,connection) => {
    if(err) throw err;
    console.log("Connected to the MongoDB cluster!");
    db = connection.db();
})
const dbIsReady = ()=>{
    return db != null;
};
const getDb = () =>{
    if(!dbIsReady())
        throw Error("No database connection");
    return db;
}
const  getUserAge = async(username)=>{
    var users = getDb().collection("users");
    var user = await users.findOne({username:username});
     if(user && user.username === username) {

     console.log("age:  " + user.age);
     return user.age
     }
     return "";
}

const  getUserCollege = async(username)=>{
    var users = getDb().collection("users");
    var user = await users.findOne({username:username});
     if(user && user.username === username) {
     console.log("college:  " + user.college);
     return user.college
     }
     return "";
}
const  getUserGender = async(username)=>{
    var users = getDb().collection("users");
    var user = await users.findOne({username:username});
     if(user && user.username === username) {
     console.log("gender:  " + user.gender);
     return user.gender
     }
     return "";
}
const getGruoprember = async(_id)=>{
    console.log("groupid check at get groupid :" + _id);
    var group=getDb().collection("group");
    var user = await group.findOne({_id:_id});
    console.log("goup rember: " + user);
    if(user!=null && user._id === _id){
    
    return user.userlist

    }
    return userlist=['Luo', 'Ming'];
}
const  checklogin = async (username,password)=>{
    var users = getDb().collection("users");
    var user = await users.findOne({username:username});
    if(user!=null && user.username==username){
        console.log("Debug>messengerdb.checklogin-> user found:\n" +
        JSON.stringify(user))
        return bcrypt.compareSync(password, user.password); 
    }
    return null
}
const queryUser = async () => {
    var users = getDb().collection("users");
    let list = await users.find({}).toArray();

    return list;
}

const addGroup = async (userlist) => {
    var group = getDb().collection("group");
    var users = getDb().collection("users");
    var newUser = {userlist: userlist};
    var result = await group.insertOne(newUser);
    /// is result is error
    let id = result.ops[0]._id;
    for (let user of userlist) {
        let res = await users.update({username: user}, {
            $set: {
                "groupid": id
            }
        })
    }
    
    return id;

}

const addUser = (username,password,age,college,gender,callback)=>{
    console.log("Debug>messengerdb.addUser:"+ username + "/" + password)
    var users = getDb().collection("users");
    users.findOne({username:username}).then(user=>{
      if(user && user.username === username) {
        console.log(`Debug>messengerdb.addUser: Username '${username}' exists!`);
        callback("UserExist");
        }
      else
      {
          var hashedpassword = bcrypt.hashSync(password,10);
          var newUser = {"username": username, "password": hashedpassword ,"age":age,"college":college,"gender":gender}
          users.insertOne(newUser,(err,result)=>{
              if(err){
                  console.log("Debug>messengerdb.addUser: error for adding '" +
                               username +"':\n", err);
                  callback("Error");
              }
              else
              {
                  console.log("Debug>messengerdb.addUser: a new user added: \n",
                               result.ops[0].username);
                  callback("Success")
              }

          })


      }

})
    //callback("a database handling message")
}

const storePrivateChat = (receiver,message)=>{
    let timestamp = Date.now();
    let chat = {receiver:receiver,message:message,timestamp:timestamp};
    getDb().collection("private_chat").insertOne(chat,function(err,doc){
        if(err!=null)
        {
            console.log(err);
        }
        else
        {
            console.log("Debug: message is added:" + JSON.stringify(doc.ops));
        }
    })
}
const storeGruopChat = (receiver,message)=>{
    let timestamp = Date.now();
    let chat = {receiver:receiver,message:message,timestamp:timestamp};
    getDb().collection("gruop_chat").insertOne(chat,function(err,doc){
        if(err!=null)
        {
            console.log(err);
        }
        else
        {
            console.log("Debug: message is added:" + JSON.stringify(doc.ops));
        }
    })
}
const loadGoupChatHistory = async (username, limits=100)=> {
    var users = getDb().collection("users");
    var user = await users.findOne({username:username});
    if(user && user.username === username) {
     console.log("groupid:  " + user.groupid);
     var groupid = user.groupid
     }
    var chat_history = await getDb().collection("gruop_chat").find({receiver:goupid}).sort({timestamp:-1}).limit(limits).toArray();
    console.log("Debug: "+ JSON.stringify(chat_history));
    if(chat_history&&chat_history.length>0)
    {
        return chat_history;
    }
}
const loadPriChatHistory = async (receiver, limits=100)=> {
    var chat_history = await getDb().collection("private_chat").find({receiver:receiver}).sort({timestamp:-1}).limit(limits).toArray();
    console.log("Debug: "+ JSON.stringify(chat_history));
    if(chat_history&&chat_history.length>0)
    {
        return chat_history;
    }
}

module.exports = {checklogin,addUser, queryUser, addGroup,getUserAge,getUserGender,getUserCollege,storePrivateChat,loadPriChatHistory,storeGruopChat,getGruoprember,loadGoupChatHistory};